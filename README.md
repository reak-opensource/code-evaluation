# Code Evaluation

Coding skills evaluation platform, A easy to use code evaluation platform with online test case checking and IDE.


## Features

* Get candidates to write code and instantly check for bug fixes, Best suited for performance reviews and hiring
* Online IDE to help write code
* UnitTest checking for candidate to run their code against predefined test cases
* Time tracker - Logs time taken by candidate to finish the code.